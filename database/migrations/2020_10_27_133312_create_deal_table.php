<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('client_id');
            $table->string('project_title')->nullable();
            $table->string('contact_person')->nullable();
            $table->longText('services_provided')->nullable();
            $table->decimal('service_price')->nullable();
            $table->longText('additional_information')->nullable();
            $table->string('other_services')->nullable();
            $table->dateTime('starting_date')->nullable();
            $table->dateTime('delivering_date')->nullable();
            $table->string('comments')->nullable();
            $table->string('lost_reason')->nullable();
            $table->longText('terms')->nullable();
            $table->enum('status', ['pending', 'won', 'lost'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
