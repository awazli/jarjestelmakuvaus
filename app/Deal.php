<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_title', 'contact_person', 'services_provided', 'service_price',
        'additional_information', 'starting_date', 'delivering_date',
        'client_id', 'comments','terms'
    ];
}
