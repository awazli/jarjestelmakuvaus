<?php

namespace App\Http\Controllers;

use App\Client;
use App\Setting;
use Illuminate\Http\Request;
use App\Deal;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;

class DealController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $clients = Client::all();
        $setting = Setting::first();
        return view('deal.create', compact('clients','setting'));
    }

    public function store(Request $request)
    {
        $rules = [
            'project_title' => 'required',
            'contact_person' => 'required',
            'services_provided' => 'required',
            'service_price' => 'required',
            'additional_information' => 'required',
//            'other_services' => 'required',
            'client_id' => 'required',
            'starting_date' => 'required',
            'delivering_date' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::to('deal/create')
                ->withErrors($validator);

        } else {
            $deal = Deal::create($request->except(['token']));
            Session::flash('message', 'Deal added successfully.');
            Session::flash('message_title', 'success');
            return redirect('deal/' . $deal->id);
        }
    }

    public function edit($id)
    {
        $clients = Client::all();
        $deal = Deal::where('id', $id)->first();
        return view('deal.edit', compact('deal', 'id', 'clients'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'project_title' => 'required',
            'contact_person' => 'required',
            'services_provided' => 'required',
            'service_price' => 'required',
            'additional_information' => 'required',
//            'other_services' => 'required',
            'client_id' => 'required',
            'starting_date' => 'required',
            'delivering_date' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::to('deal/' . $id . '/edit')
                ->withErrors($validator);

        } else {
            Deal::where('id', $id)->update($request->except(['_token', '_method', 'files']));
            Session::flash('message', 'Deal updated successfully.');
            Session::flash('message_title', 'success');
            return redirect('home');
        }
    }

    public function show($id)
    {
        $deal = Deal::where('id', $id)->first();
        $client = Client::where('id', $deal->client_id)->first();
        $deal['starting_date'] = Carbon::parse($deal['starting_date'])->format('d-m-Y');
        $deal['delivering_date'] = Carbon::parse($deal['delivering_date'])->format('d-m-Y');
        return view('deal.view', compact('deal', 'client'));
    }

    public function downloadPdf($id)
    {
        $deal = Deal::where('id', $id)->first();
        $client = Client::where('id', $deal->client_id)->first();
        $company = Setting::first();
        $data = [
            'deal' => $deal,
            'company' => $company,
            'client' => $client
        ];

        $pdf = PDF::loadView('deal.pdf', $data);
        return $pdf->download($client->name . '.pdf');
    }

    public function sendEmail($id)
    {
        $deal = Deal::where('id', $id)->first();
        $client = Client::where('id', $deal->client_id)->first();
        $company = Setting::first();
        $data = [
            'deal' => $deal,
            'company' => $company,
            'client' => $client
        ];

        $destinationPath = 'pdf/';
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0755, true);
        }

        $pdf = PDF::loadView('deal.pdf', $data);
        $pdf->save($destinationPath . $client->name . '.pdf');

        try {
            Mail::send('deal.email', $data, function ($message) use ($client) {
                $message->to($client->email)
                    ->subject('Deal Overview')
                    ->attach($destinationPath . $client->name . '.pdf', [
                        'as' => 'agreement.pdf',
                        'mime' => 'application/pdf'
                    ]);
            });
        } catch (\Exception $e) {

        }

        Session::flash('message', 'Email sent successfully.');
        Session::flash('message_tittle', 'success');
        return redirect('home');
    }
}