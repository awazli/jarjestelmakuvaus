<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\File;
use DataTables;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $clients = Client::all();
        return view('client.index', compact('clients'));
    }

    public function getClients(Request $request)
    {
        if ($request->ajax()) {
            $clients = Client::all();
            foreach ($clients as $client) {
                $route = route('client.edit', $client->id);
                $edit = __('datatable.edit');
                $delete = __('datatable.delete');

                $client->action = '<button type="button" style="margin-right:5px;"
                                                class="btn btn-blue btn-create"><a href="' . $route . '">' . $edit . '</a></button> <button type="button" class="btn btn-blue btn-create delete-client" data-id=' . $client->id . '><a href="javascript:void(0);">' . $delete . '</a></button>';
            }
            return DataTables::of($clients)
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function create()
    {
        return view('client.create');
    }

    public function store(Request $request)
    {
        $url = URL::previous();
        $parts = explode("/", $url);
        $token = end($parts);
        $rules = array(
            'name' => 'required',
            'contact_no' => 'required',
            'email' => 'required',
            'address' => 'required',
            'billing_address' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();
            if ($token === "deal") {
                return Redirect::to('client/create/deal')
                    ->withErrors($validator);
            } else {
                return Redirect::to('client/create')
                    ->withErrors($validator);
            }
        } else {
            Client::create($request->except(['token']));
            Session::flash('message', 'Client added successfully.');
            Session::flash('message_title', 'success');
            if ($token === "deal") {
                return Redirect::to('deal/create');
            } else {
                return redirect('clients');
            }
        }
    }

    public function edit($id)
    {
        $client = Client::where('id', $id)->first();
        return view('client.edit', compact('client', 'id'));
    }

    public function update($id, Request $request)
    {
        $rules = array(
            'name' => 'required',
            'contact_no' => 'required',
            'email' => 'required',
            'address' => 'required',
            'billing_address' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::to('client/' . $id . '/edit')
                ->withErrors($validator);

        } else {
            Client::where('id', $id)->update($request->except(['_token', '_method']));
            Session::flash('message', 'Client updated successfully.');
            Session::flash('message_title', 'success');
            return redirect('clients');
        }
    }

    public function destroy($id)
    {
        Client::where('id', $id)->delete();
        Session::flash('message', 'Client deleted successfully.');
        Session::flash('message_title', 'success');
        return redirect('clients');
    }
}