<?php

namespace App\Http\Controllers;

use App\Client;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Deal;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $deals = Deal::all();
        $pending['count'] = Deal::where('status', 'pending')->count();
        $pending['sum']= Deal::where('status','pending')->sum('service_price');
        $won['count'] = Deal::where('status', 'won')->count();
        $won['sum'] = Deal::where('status', 'won')->sum('service_price');
        $lost['count'] = Deal::where('status', 'lost')->count();
        $lost['sum'] = Deal::where('status', 'lost')->sum('service_price');

//        foreach ($deals as $deal) {
//            $client = Client::where('id', $deal->client_id)->first();
//            $deal->client_name = $client ? $client->name : "";
//            $deal->delivering_date = Carbon::parse($deal->delivering_date)->format('d-m-Y');
//        }

        return view('home', compact( 'pending', 'won', 'lost'));
    }

    public function getDeals(Request $request){
        if ($request->ajax()) {
            $deals = Deal::all();
            foreach ($deals as $deal) {
                $client = Client::where('id', $deal->client_id)->first();
                $deal->client_name = $client ? $client->name : "";
                $deal->delivering_date = Carbon::parse($deal->delivering_date)->format('d-m-Y');
                $route = route('deal.edit',$deal->id);
                $edit = __('datatable.edit');
                if($deal->status === 'pending'){
                    $deal->action = '<button type="button" class="btn btn-blue btn-create"><a
                                                        href="'.$route.'">'.$edit.'</a></button>';
                }else{
                    $deal->action = '<span>-</span>';
                }
                $deal->status = ucwords($deal->status);
            }
            return DataTables::of($deals)
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function settings()
    {
        $setting = Setting::first();
        return view('settings', compact('setting'));
    }

    public function updateSettings(Request $request)
    {
        $info = Setting::first();
        if ($request->hasFile('logo')) {
            if ($info && $info->logo) {
                unlink('company/' . $info->logo);
            }

            $file = $request->file('logo');
            $destinationPath = 'company/';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }

            $filename = str_replace(' ', '', $file->getClientOriginalName());
            $file->move($destinationPath, $filename);

            if ($info) {
                Setting::where('id', $info->id)->update([
                    'logo' => $filename
                ]);
            } else {
                Setting::create([
                    'logo' => $filename
                ]);
            }
        }

        if ($info) {
            Setting::where('id', $info->id)->update([
                'email' => $request->get('email'),
                'about' => $request->get('about')
            ]);
        } else {
            Setting::create([
                'email' => $request->get('email'),
                'about' => $request->get('about')
            ]);
        }

        Session::flash('message', 'Account settings updated successfully.');
        Session::flash('message_title', 'success');
        return redirect('settings');
    }

    public function terms()
    {
        $setting = Setting::first();
        return view('terms', compact('setting'));
    }

    public function updateTerms(Request $request)
    {
        $info = Setting::first();

        if ($info) {
            Setting::where('id', $info->id)->update([
                'terms' => $request->get('terms')
            ]);
        } else {
            Setting::create([
                'terms' => $request->get('terms')
            ]);
        }

        Session::flash('message', 'Terms updated successfully.');
        Session::flash('message_title', 'success');
        return redirect('terms');
    }
}
