$("#clientForm").validate({
    rules: {
        'name': "required",
        'contact_no': "required",
        'email': "required",
        'address': "required",
        'billing_address': "required",
    },
    // messages: {
    //     'name': "Client name is required",
    //     'contact_no': "Contact no is required",
    //     'email': "Email is required",
    //     'address': "Address is required",
    //     'billing_address': "Billing address is required"
    // },
    messages: {
        'name': "Tarkista kentän tiedot",
        'contact_no': "Tarkista kentän tiedot",
        'email': "Tarkista kentän tiedot",
        'address': "Tarkista kentän tiedot",
        'billing_address': "Tarkista kentän tiedot"
    },
    submitHandler: function (form) {
        form.submit();
    }
});

$("#dealForm").validate({
    rules: {
        'client_id': "required",
        'project_title': "required",
        'contact_person': "required",
        'services_provided': "required",
        'service_price': "required",
        'additional_information': "required",
        // 'other_services': "required",
        'starting_date': "required",
        'delivering_date': "required"
    },
    messages: {
        'client_id': "Please select a client",
        'project_title': "Project title is required",
        'contact_person': "Contact person is required",
        'services_provided': "Services Provided is required",
        'service_price': "Service Price is required",
        'additional_information': "Additional Information is required",
        // 'other_services': "Other Services is required",
        'starting_date': "starting Date is required",
        'delivering_date': "Delivering Date is required"
    },
    submitHandler: function (form) {
        form.submit();
    }
});