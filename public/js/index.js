$('.proifle_content').click(function(){
  $('.drop_down_menu').toggleClass('show');
});

$('#status').on("change",function(){
  var status = $(this).val();
  if(status === "lost"){
    $("#status_div").removeAttr('style').show();
  }else{
      $('#lost_reason').val("");
      $("#status_div").removeAttr('style').hide();
  }
});