@extends('layouts.app')

@section('content')
    <div class="form-col">
        <h4>
            Reset Password
        </h4>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-wrapper">
                <label for="email" class="col-form-label">{{ __('Email') }}</label>

                <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email"
                       value="{{ old('email') }}" required autocomplete="email" autofocus
                       placeholder="Enter your email id">

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-wrapper-bottom row">
                <div class="col-md-8">
                    <label class="reset_text">
                        Have an Account? <a href="{{ route('login') }}">Sign in</a>
                    </label>
                </div>

                <div class="col-md-4">
                    <button type="submit" class="btn btn-submit">
                        {{__('Reset')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection