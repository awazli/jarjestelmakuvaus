@extends('layouts.app')

@section('content')
    <div class="form-col">
        <h4>
            {{__("Confirm Password")}}
        </h4>
        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-wrapper">
                <label for="email" class="col-form-label">{{ __('Email') }}</label>


                <input id="email" type="email" class="@error('email') is-invalid @enderror"
                       name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-wrapper">
                <label for="password" class="col-form-label">{{ __('Password') }}</label>

                <input id="password" type="password"
                       class="@error('password') is-invalid @enderror" name="password"
                       required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-wrapper">
                <label for="password-confirm"
                       class="col-form-label">{{ __('Confirm Password') }}</label>

                <input id="password-confirm" type="password" name="password_confirmation" required
                       autocomplete="new-password">

            </div>
            <div class="form-wrapper-bottom row">
                <div class="col-md-8">
                    <label class="reset_text">
                        Have an Account? <a href="{{route('login')}}">Sign in</a>
                    </label>
                </div>

                <div class="col-md-4">
                    <button type="submit" class="btn btn-submit">
                        {{ __('Submit') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection