@extends('layouts.app')

@section('content')
    <div class="form-col">
        <h4>
            {{__('auth.login')}}
        </h4>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-wrapper">
                <label for="email"
                       class="col-form-label">{{ __('auth.email') }}</label>

                <input id="email" type="email"
                       class="@error('email') is-invalid @enderror" name="email"
                       placeholder="{{__('auth.email_placeholder')}}"
                       value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-wrapper">
                <label for="password"
                       class="col-form-label">{{ __('auth.password') }}</label>

                <input id="password" type="password"
                       class="@error('password') is-invalid @enderror" name="password"
                       placeholder="{{__('auth.password_placeholder')}}"
                       required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror

            </div>
            <div class="form-wrapper-bottom row">
                <div class="col-md-8">
                    <label class="form-check-label" for="remember">
                        <input class="form-check-input" type="checkbox" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span> {{ __('auth.remember_me') }}</span>
                    </label>
                </div>

                <div class="col-md-4">
                    <button type="submit" class="btn btn-submit">
                        {{ __('auth.login') }}
                    </button>
                </div>
            </div>
            @if (Route::has('password.request'))
                <a class="btn btn-link pl-0" href="{{ route('password.request') }}">
                    {{ __('auth.forgot_password') }}
                </a>
            @endif
        </form>
    </div>
@endsection