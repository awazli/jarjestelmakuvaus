@extends('layouts.master')
@section('content')
    <div class="top-b" style="">
        <div class="container">
             <button class="back_btn"><a href="{{route('home')}}"><i class="fa fa-long-arrow-left mr-3" aria-hidden="true"></i> {{__('auth.go_back')}}</a>
            </button>
        </div>
    </div>
    <div class="company">
        <div class="container">
           
            <div class="row company_top">
                <div class="col-md-4">
                    <h3>{{__('client.clients')}}</h3>
                </div>
                <div class="col-md-8 text-right">
                    <button class="sort_btn" type="button"><i class="sort-btn"></i></button>
                    <button type="button" class="btn btn-blue btn-create"><a
                                href="/client/create?deal=0">{{__('client.create_client')}}</a>
                    </button>
                </div>
            </div>
            <div class="data-table-responsive">
                <div class="data-content-wrapper">
                   
                    {{--<div class="data-table-content">--}}
                        {{--<div class="data-table-header">--}}
                            {{--<div class="row">--}}
                                {{--<div class="co-md-2 col-5 scol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('client.client')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-2 col-5 thcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('client.company_name')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-2 col-5 frcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('datatable.contact')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-2 col-5 fvcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('auth.email')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-2 col-5 fvcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('datatable.action')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@foreach($clients as $client)--}}
                            {{--<div class="data-table-list">--}}
                                {{--<div class="row data-row">--}}
                                    {{--<div class="co-md-2 col-5 scol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ $client->name }}</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-2 col-5 thcol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ $client->company_name }}</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-2 col-5 frcol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ $client->contact_no }}</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-2 col-5 fvcol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ $client->email }}</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-2 col-5 fvcol" style="display: inline-flex">--}}
                                        {{--<button type="button" style="margin-right: 5px;"--}}
                                                {{--class="btn btn-blue btn-create"><a--}}
                                                    {{--href="{{route('client.edit',[$client->id])}}">--}}
                                                {{--{{__('datatable.edit')}}</a>--}}
                                        {{--</button>--}}
                                        {{--<button data-id="{{ $client->id }}" type="button"--}}
                                                {{--class="btn btn-blue btn-create delete-client"><a--}}
                                                    {{--href="javascript:void(0);">--}}
                                                {{--{{__('datatable.delete')}}</a>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                    <table class="table table-bordered client-list">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>{{__('client.client')}}</th>
                            <th>{{__('client.company_name')}}</th>
                            <th>{{__('datatable.contact')}}</th>
                            <th>{{__('auth.email')}}</th>
                            <th>{{__('datatable.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('.client-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('client.list') }}",
                    type: 'get'
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'company_name', name: 'company_name'},
                    {data: 'contact_no', name: 'contact_no'},
                    {data: 'email', name: 'email'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });
            $(".delete-client").click(function () {
                let id = $(this).data('id');
                $.confirm({
                    title: '',
                    content: 'Oletko varma, että haluat poistaa asiakkaan kokonaan?',
                    columnClass: 'medium',
                    buttons: {
                        "Vahvista & Poista": function () {
                            $.ajax({
                                type: "get",
                                url: "/client/" + id,
                                data: {},
                                success: function (response) {
                                    $.growl.notice({title: "", message: "Record deleted successfully."});
                                    window.location.reload();
                                }
                            });
                        },
                        "Peruuta": function () {

                        }
                    }
                });
            });
        })
    </script>
@endsection
