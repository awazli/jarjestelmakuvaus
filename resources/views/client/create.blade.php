@extends('layouts.master')
@section('content')
    <div class="create_area">
        <div class="container position-relative">
            <button class="back_btn" onclick="window.history.back();"><i class="back_icon"></i> {{__('auth.back')}}</button>
            <div class="create_form_wrapper">
                <div class="create_form_content_box">
                    <div class="form_header text-center">
                        <h3>{{__('client.add_client')}}</h3>
                        <p>
                            {{__('client.multiple_user')}}
                        </p>
                    </div>
                    <div class="form-content">
                        <form id="clientForm" method="POST" action="{{ route('client.store') }}">
                            @csrf
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>{{__('client.name')}}</label>
                                    <input type="text" name="name" placeholder="{{__('client.name')}}"
                                           class="@error('name') is-invalid @enderror"/>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>{{__('client.company_name')}}</label>
                                    <input type="text" name="company_name" placeholder="{{__('client.company_name')}}"/>
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>{{__('client.vat_id')}}</label>
                                    <input type="text" name="vat_id" placeholder="{{__('client.vat_id')}}"/>
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-6">
                                    <label>{{__('datatable.contact_no')}}</label>
                                    <input type="text" name="contact_no" placeholder="{{__('datatable.contact_no')}}"
                                           class="@error('contact_no') is-invalid @enderror"/>
                                    @error('contact_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label>{{__('auth.email')}}</label>
                                    <input type="email" name="email" placeholder="{{__('auth.email')}}"
                                           class="@error('email') is-invalid @enderror"/>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>{{__('client.address')}}</label>
                                    <input type="text" name="address" placeholder="{{__('client.address')}}"
                                           class="@error('address') is-invalid @enderror"/>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>{{__('client.billing_address')}}</label>
                                    <input type="text" name="billing_address" placeholder="{{__('client.billing_address')}}"
                                           class="@error('billing_address') is-invalid @enderror"/>
                                    @error('billing_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="btn-submit-row">
                                <button type="submit" class="btn btn-block btn-blue">
                                    {{__('client.create_client')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection