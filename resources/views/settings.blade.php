@extends('layouts.master')
@section('content')
    <div class="create_area">
        <div class="container position-relative">
            <button class="back_btn" onclick="window.location.href='/'"><i class="back_icon"></i> Back</button>
            <div class="create_form_wrapper">
                <div class="create_form_content_box">
                    <div class="form_header text-center">
                        <h3>Account Settings</h3>
                    </div>
                    <div class="form-content">
                        <form id="settingsForm" enctype="multipart/form-data" method="POST"
                              action="{{ route('settings.update') }}">
                            @csrf
                            <div class="form-wrap-row row">
                                <div class="col-md-9">
                                    <label>Logo</label>
                                    <input type="file" accept="image/*" name="logo"/>
                                </div>
                                <div class="col-md-3">
                                    @if($setting && $setting->logo)
                                        <img src="{{ asset('company/'.$setting->logo) }}" height="100" width="100"/>
                                    @endif
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>Email</label>
                                    <input type="text" name="email" placeholder="Email"
                                           value="{{ $setting ? $setting->email : "" }}"/>
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>About</label>
                                    <textarea name="about" rows="5"
                                              class="form-control">{{ $setting ? $setting->about : "" }}</textarea>
                                </div>
                            </div>
                            <div class="btn-submit-row">
                                <button type="submit" class="btn btn-block btn-blue">
                                    Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection