@extends('layouts.master')
@section('content')
    <div class="company">
        <div class="container">
            <div class="row company_top">
                <div class="col-md-4">
                    <h3>{{__('dashboard.deals')}}</h3>
                </div>
                <div class="col-md-8 text-right">
                    <button type="button" class="btn btn-blue btn-create"><a
                                href="{{route('terms.index')}}">{{__('dashboard.sales_terms')}}</a>
                    </button>
                    <button type="button" class="btn btn-blue btn-create"><a
                                href="{{route('client.index')}}">{{__('dashboard.clients_management')}}</a>
                    </button>
                    <button type="button" class="btn btn-success btn-su btn-create"><a
                                href="{{route('deal.create')}}">{{__('dashboard.create_deal')}}</a>
                    </button>
                </div>
            </div>
            <div class="company_info_card row">
                <div class="col-md-4">
                    <div class="col_content text-center all_deals">
                        <img src="{{ asset('img/Group 2.svg') }}">
                        <h4>
                            {{ $pending['count'] }}
                        </h4>
                        <p>
                            {{__('dashboard.pending_deals')}}
                        </p>
                        <p>
                            {{ $pending['sum'] }}<i class="fa fa-eur"></i>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col_content text-center won_deals">
                        <img src="{{asset('img/Group 3.svg')}}">
                        <h4>
                            {{ $won['count'] }}
                        </h4>
                        <p>
                            {{__('dashboard.won_deals')}}
                        </p>
                        <p>
                            {{ $won['sum'] }}<i class="fa fa-eur"></i>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col_content text-center lost_deals">
                        <img src="{{asset('img/x-circle.svg')}}">
                        <h4>
                            {{ $lost['count'] }}
                        </h4>
                        <p>
                            {{__('dashboard.lost_deals')}}
                        </p>
                        <p>
                            {{ $lost['sum'] }}<i class="fa fa-eur"></i>
                        </p>
                    </div>
                </div>
            </div>
            <div class="data-table-responsive">
                <div class="data-content-wrapper">

                    {{--<div class="data-table-content">--}}
                        {{--<div class="data-table-header">--}}
                            {{--<div class="row">--}}
                                {{--<div class="co-md-2 col-2 fcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('dashboard.project_title')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-2 col-2 scol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('datatable.contact')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-2 col-2 thcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('dashboard.client_name')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-2 col-2 frcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('dashboard.close_date')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-1 col-1 fvcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('dashboard.value')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-1 col-1 fvcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('datatable.status')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="co-md-1 col-1 fvcol">--}}
                                    {{--<label>--}}
                                        {{--<span>{{__('datatable.action')}}</span>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="data-table-list">--}}
                            {{--@foreach($deals as $deal)--}}
                                {{--<div class="row data-row">--}}
                                    {{--<div class="co-md-2 col-2 fcol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ $deal->project_title }}</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-2 col-2 scol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ $deal->contact_person }}</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-2 col-2 thcol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ $deal->client_name }}</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-2 col-2 frcol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ $deal->delivering_date }}</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-1 col-1 fvcol">--}}
                                        {{--<label>--}}
                                            {{--<span>${{ $deal->service_price }}</span></span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-1 col-1 fvcol">--}}
                                        {{--<label>--}}
                                            {{--<span>{{ ucwords($deal->status) }}</span></span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="co-md-1 col-1 fvcol">--}}
                                        {{--@if($deal->status === 'pending')--}}
                                            {{--<button type="button" class="btn btn-blue btn-create"><a--}}
                                                        {{--href="{{route('deal.edit',[$deal->id])}}">--}}
                                                    {{--{{__('datatable.edit')}}</a>--}}
                                            {{--</button>--}}
                                        {{--@else--}}
                                            {{--<span>-</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <table class="table table-bordered deal-list">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>{{__('dashboard.project_title')}}</th>
                            <th>{{__('datatable.contact')}}</th>
                            <th>{{__('dashboard.client_name')}}</th>
                            <th>{{__('dashboard.close_date')}}</th>
                            <th>{{__('dashboard.value')}}</th>
                            <th>{{__('datatable.status')}}</th>
                            <th>{{__('datatable.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('.deal-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('deal.list') }}",
                    type: 'get'
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'project_title', name: 'project_title'},
                    {data: 'contact_person', name: 'contact_person'},
                    {data: 'client_name', name: 'client_name'},
                    {data: 'delivering_date', name: 'delivering_date'},
                    {data: 'service_price', name: 'service_price'},
                    {data: 'status', name: 'status'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });

        });
    </script>
@endsection

