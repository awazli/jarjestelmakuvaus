<!DOCTYPE html>
<html lang="en-GB">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Deal Management</title>
</head>
<style type="text/css" media="all">
    @page {
        margin-top: 150px !important;
        margin-left: 0px !important;
        margin-right: 0px !important;
        margin-bottom: 0px !important;
    }

    header {
        position: fixed;
        top: -100px;
        left: 50px;
        right: 0px;
        height: 200px;
        font-size: 16px !important;
    }

    body {
        zoom: 0.9;
        -ms-zoom: 0.9;
        -webkit-zoom: 0.9;
        -moz-transform: scale(0.9, 0.9);
        -moz-transform-origin: center top;
        font-size: 18px;
        line-height: 24px;
        color: #333;
        padding-top: 30px !important;
        margin: 0px !important;
    }

    * {
        box-sizing: border-box;
    }

    img {
        max-width: 100%;
    }
</style>
<body>
<header>
    <div class="top-right-image" style="position: absolute;
    left: 0;
    top: 0;
    width: 200px;
    height: 100px;
    z-index: 1;">
        @if($company && $company->logo)
            <img src="{{ url('company/'.$company->logo) }}" alt=""/>
        @endif
        <p>{{ $client->vat_id }}</p>
    </div>
</header>
<main>
    <div class="create_area" style="background: #3FABFF;">
        <div class="container position-relative">
            <div class="create_form_wrapper"
                 style="background: #FFFFFF;width: 100%;margin: 50px !important;margin-top: 50px;border-radius: 10px;padding: 30px;">
                <div class="create_form_content_box">
                    <div class="form-content">
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="project_title">Project Title</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->project_title}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="client">Client Name</label>
                            </div>
                            <div class="col-md-9">
                                {{$client->name}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="contact_person">Contact Person</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->contact_person}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="services_provided">Services Provided</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->services_provided}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="service_price">Services Price</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->service_price}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="additional_information">Additional Information</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->additional_information}}
                            </div>
                        </div>

                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="other_services">Other Services</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->other_services}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="starting_date">Starting Date</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->starting_date}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="delivering_date">Delivering Date</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->delivering_date}}
                            </div>
                        </div>
                        <div class="form-wrap-row row pb-x">
                            <div class="col-md-3">
                                <label for="comments">Comments</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->comments}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
</html>