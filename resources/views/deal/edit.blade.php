@extends('layouts.master')
@section('content')

    <!--Create Area-->
    <div class="create_area">
        <div class="container position-relative">
            <button class="back_btn"><a href="{{route('home')}}"><i class="back_icon"></i> Back</a></button>

            <div class="create_form_wrapper">
                <div class="create_form_content_box">
                    <div class="form_header text-center">
                        <h3>Edit Deal</h3>
                    </div>

                    <div class="form-content">
                        <form id="dealForm" name="dealForm" method="POST"
                              action="{{ route('deal.update', ['id' => $id]) }}">
                            <input type="hidden" name="_method" value="put"/>
                            @csrf
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label for="project_title" class="col-form-label">Project Title</label>
                                    <input type="text" name="project_title" id="project_title"
                                           value="{{ $deal->project_title }}"
                                           class="@error('project_title') is-invalid @enderror"
                                           placeholder="Project Title">
                                    @error('project_title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-6">
                                    <label for="contact_person" class="col-form-label">Contact Person</label>
                                    <input type="text" name="contact_person"
                                           value="{{ $deal->contact_person }}" id="contact_person"
                                           class="@error('contact_person') is-invalid @enderror"
                                           placeholder="Contact Person">
                                    @error('contact_person')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="client" class="col-form-label">Client</label>
                                    <select name="client_id">
                                        <option value="">Select Client</option>
                                        @foreach($clients as $client)
                                            <option {{ $client->id === $deal->client_id ? 'selected' : '' }}
                                                    value="{{ $client->id }}">
                                                {{ $client->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('client_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label for="services_provided" class="col-form-label">Services Provided</label>
                                    <textarea placeholder="Services Provided" class="form-control"
                                              name="services_provided">{{ $deal->services_provided }}</textarea>
                                    @error('services_provided')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label for="additional_information" class="col-form-label">Additional
                                        Information</label>
                                    <textarea placeholder="Additional Information" class="form-control"
                                              name="additional_information">{{ $deal->additional_information }}</textarea>
                                    @error('additional_information')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-6">
                                    <label for="service_price" class="col-form-label">Services Price</label>
                                    <input type="text" name="service_price" id="service_price"
                                           value="{{ $deal->service_price }}"
                                           class="@error('service_price') is-invalid @enderror"
                                           placeholder="Services Price">
                                    @error('service_price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                {{--<div class="col-md-6">--}}
                                    {{--<label for="other_services" class="col-form-label">Other Services</label>--}}
                                    {{--<input type="text" name="other_services" id="other_services"--}}
                                           {{--value="{{ $deal->other_services }}"--}}
                                           {{--class="@error('other_services') is-invalid @enderror"--}}
                                           {{--placeholder="Other Services">--}}
                                    {{--@error('other_services')--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@enderror--}}
                                {{--</div>--}}
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-6">
                                    <label for="starting_date" class="col-form-label">Starting Date</label>
                                    <input type="date" name="starting_date" id="starting_date"
                                           value="{{ \Carbon\Carbon::parse($deal->starting_date)->format('Y-m-d') }}"
                                           class="@error('starting_date') is-invalid @enderror"
                                           placeholder="DD/MM/YYYY">
                                    @error('starting_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="delivering_date" class="col-form-label">Delivering Date</label>
                                    <input type="date" name="delivering_date" id="delivering_date"
                                           value="{{ \Carbon\Carbon::parse($deal->delivering_date)->format('Y-m-d') }}"
                                           class="@error('delivering_date') is-invalid @enderror"
                                           placeholder="DD/MM/YYYY">
                                    @error('delivering_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label for="comments" class="col-form-label">Comments</label>
                                    <textarea name="comments" class="form-control" placeholder="Comments">{{ $deal->comments }}</textarea>
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>{{__('dashboard.terms')}}</label>
                                    <div id="terms-editor">{!! $deal ? $deal->terms : "" !!}</div>
                                    <input name="terms" id="terms" type="hidden"/>
                                </div>
                            </div>
                            @if($deal->status === 'pending')
                                <div class="form-wrap-row row pb-x">
                                    <div class="col-md-6">
                                        <label for="status" class="col-form-label">Status</label>
                                        <select name="status" id="status">
                                            <option value="won">Won</option>
                                            <option value="lost">Lost</option>
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="form-wrap-row row">
                                <div id="status_div" class="col-md-12" style="display:none">
                                    <label for="lost_reason" class="col-form-label">Reason</label>
                                    <textarea name="lost_reason" id="lost_reason" class="form-control overflow-hidden" placeholder="Lost reason">{{ $deal->lost_reason }}</textarea>
                                </div>
                            </div>
                            <div class="btn-submit-row">
                                <button type="submit" class="btn btn-block btn-blue">Update
                                    Deal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Create Area-->

@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#terms-editor').summernote({
                height: 250
            });
        });

        $("#dealForm").validate({
            rules: {},
            messages: {},
            submitHandler: function (form) {
                $("#terms").val($('#terms-editor').summernote('code'));
                form.submit();
            }
        });
    </script>
@endsection