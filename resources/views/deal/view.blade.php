@extends('layouts.master')
@section('content')
    <div class="create_area">
        <div class="container position-relative">
            <button class="back_btn"><a href="{{route('home')}}"><i class="back_icon"></i> Back</a></button>

            <div class="create_form_wrapper">
                <div class="create_form_content_box">
                    <div class="form-content">
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="client">Client Name</label>
                            </div>
                            <div class="col-md-9">
                                {{$client->name}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="project_title">Project Title</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->project_title}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="contact_person">Contact Person</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->contact_person}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="services_provided">Services Provided</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->services_provided}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="service_price">Services Price</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->service_price}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="additional_information">Additional Information</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->additional_information}}
                            </div>
                        </div>

                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="other_services">Other Services</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->other_services}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="starting_date">Starting Date</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->starting_date}}
                            </div>
                        </div>
                        <div class="form-wrap-row row">
                            <div class="col-md-3">
                                <label for="delivering_date">Delivering Date</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->delivering_date}}
                            </div>
                        </div>
                        <div class="form-wrap-row row pb-x">
                            <div class="col-md-3">
                                <label for="comments">Comments</label>
                            </div>
                            <div class="col-md-9">
                                {{$deal->comments}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{route('deal.pdf', [$deal->id])}}">
                                    <button class="btn btn-block btn-blue">Download PDF
                                    </button>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('deal.email', [$deal->id])}}">
                                    <button class="btn btn-block btn-blue">Send By Email
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection