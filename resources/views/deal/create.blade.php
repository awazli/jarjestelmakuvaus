@extends('layouts.master')
@section('content')
    <div class="create_area">
        <div class="container position-relative">
            <button class="back_btn"><a href="{{route('home')}}"><i class="back_icon"></i> Back</a></button>
            <div class="create_form_wrapper">
                <div class="create_form_content_box">
                    <div class="form_header text-center">
                        <h3>Add New Deal</h3>
                        <p>
                            Multiple users? we offer custom tailored packages including premium support and detailed
                            analytics.
                        </p>
                    </div>
                    <div class="form-content">
                        <form id="dealForm" name="dealForm" method="POST" action="{{ route('deal.store') }}">
                            @csrf
                            <div class="form-wrap-row row">
                                <div class="col-md-6">
                                    <label for="project_title" class="col-form-label">Project Title</label>
                                    <input type="text" name="project_title" id="project_title"
                                           class="@error('project_title') is-invalid @enderror"
                                           placeholder="Project Title">
                                    @error('project_title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="contact_person" class="col-form-label">Contact Person</label>
                                    <input type="text" name="contact_person" id="contact_person"
                                           class="@error('contact_person') is-invalid @enderror"
                                           placeholder="Contact Person">
                                    @error('contact_person')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-8">
                                    <label for="client" class="col-form-label">Client</label>
                                    <select name="client_id">
                                        <option value="">Select Client</option>
                                        @foreach($clients as $client)
                                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('client_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-4" style="margin-top: 35px">
                                    <a href="{{ route('client.create','deal') }}" class="btn btn-success"
                                       style="color: white">Add
                                        New Client
                                    </a>
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label for="services_provided" class="col-form-label">Services Provided</label>
                                    <textarea placeholder="Services Provided" class="form-control"
                                              name="services_provided"></textarea>
                                    @error('services_provided')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label for="additional_information" class="col-form-label">Additional
                                        Information</label>
                                    <textarea placeholder="Additional Information" class="form-control"
                                              name="additional_information"></textarea>
                                    @error('additional_information')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-6">
                                    <label for="service_price" class="col-form-label">Services Price</label>
                                    <input type="text" name="service_price" id="service_price"
                                           class="@error('service_price') is-invalid @enderror"
                                           placeholder="Services Price">
                                    @error('service_price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                {{--<div class="col-md-6">--}}
                                    {{--<label for="other_services" class="col-form-label">Other Services</label>--}}
                                    {{--<input type="text" name="other_services" id="other_services"--}}
                                           {{--class="@error('other_services') is-invalid @enderror"--}}
                                           {{--placeholder="Other Services">--}}
                                    {{--@error('other_services')--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@enderror--}}
                                {{--</div>--}}
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-6">
                                    <label for="starting_date" class="col-form-label">Starting Date</label>
                                    <input type="date" name="starting_date" id="starting_date"
                                           class="@error('starting_date') is-invalid @enderror"
                                           placeholder="DD/MM/YYYY">
                                    @error('starting_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="delivering_date" class="col-form-label">Delivering Date</label>
                                    <input type="date" name="delivering_date" id="delivering_date"
                                           class="@error('delivering_date') is-invalid @enderror"
                                           placeholder="DD/MM/YYYY">
                                    @error('delivering_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-wrap-row row pb-x">
                                <div class="col-md-12">
                                    <label for="comments" class="col-form-label">Comments</label>
                                    <textarea name="comments" class="form-control" placeholder="Comments"></textarea>
                                </div>
                            </div>
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>{{__('dashboard.terms')}}</label>
                                    <div id="terms-editor">{!! $setting ? $setting->terms : "" !!}</div>
                                    <input name="terms" id="terms" type="hidden"/>
                                </div>
                            </div>
                            <div class="btn-submit-row">
                                <button type="submit" class="btn btn-block btn-blue">Create
                                    Deal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#terms-editor').summernote({
                height: 250
            });
        });

        $("#dealForm").validate({
            rules: {},
            messages: {},
            submitHandler: function (form) {
                $("#terms").val($('#terms-editor').summernote('code'));
                form.submit();
            }
        });
    </script>
@endsection