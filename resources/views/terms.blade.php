@extends('layouts.master')
@section('content')
    <div class="create_area">
        <div class="container position-relative">
            <button class="back_btn" onclick="window.location.href='/'"><i class="back_icon"></i> {{__('auth.back')}}</button>
            <div class="create_form_wrapper">
                <div class="create_form_content_box">
                    <div class="form_header text-center">
                        <h3>{{__('dashboard.sales_terms')}}</h3>
                    </div>
                    <div class="form-content">
                        <form id="termsForm" method="POST"
                              action="{{ route('terms.update') }}">
                            @csrf
                            <div class="form-wrap-row row">
                                <div class="col-md-12">
                                    <label>{{__('dashboard.terms')}}</label>
                                    <div id="terms-editor">{!! $setting ? $setting->terms : "" !!}</div>
                                    <input name="terms" id="terms" type="hidden"/>
                                </div>
                            </div>
                            <div class="btn-submit-row">
                                <button type="submit" class="btn btn-block btn-blue">
                                    {{__('auth.save')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#terms-editor').summernote({
                height: 250
            });
        });

        $("#termsForm").validate({
            rules: {},
            messages: {},
            submitHandler: function (form) {
                $("#terms").val($('#terms-editor').summernote('code'));
                form.submit();
            }
        });
    </script>
@endsection