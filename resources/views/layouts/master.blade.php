<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    <link href="//db.onlinewebfonts.com/c/860c3ec7bbc5da3e97233ccecafe512e?family=Circular+Std+Book" rel="stylesheet"
          type="text/css"/>
    <!-- Styles -->
    <link type="text/css" href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{asset('css/jquery.growl.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/jquery-confirm.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
            integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
            crossorigin="anonymous"></script>
    <script src="{{asset('js/jquery-confirm.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="{{asset('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

    {{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}
</head>
<body>
<div id="app">
    <!--Header-->
    <header>
        <div class="container-fluid header_wrapper">
            <div class="row">
                <div class="col-md-2 dashboardlogo">
                    <a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}"></a>
                </div>
                <div class="col-md-2 text-right">
                    {{--<li><a href="{{ url('locale/en') }}" ><i class="fa fa-language"></i> {{ __('auth.en') }}</a></li>--}}

                    {{--<li><a href="{{ url('locale/fr') }}" ><i class="fa fa-language"></i> {{ __('auth.fr') }}</a></li>--}}
                </div>
                <div class="col-md-8 text-right">
                    <div class="profile">
                        <div class="proifle_content">
                            <img src="{{ asset('img/items.png') }}"> <span>{{ \Illuminate\Support\Facades\Auth::user()->name }}
                                <i class="chevron_down"></i></span>
                        </div>
                    </div>
                    <div class="drop_down_menu">
                        <ul>
                            <li>
                                <a href="{{ route('settings.index') }}">{{__('auth.account_settings')}}</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('auth.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @if(\Illuminate\Support\Facades\Session::has('message'))
            <script>
                $(function () {
                    $.growl.notice({
                        title: "{!! \Illuminate\Support\Facades\Session::get('message_tittle') !!}",
                        message: "{!! \Illuminate\Support\Facades\Session::get('message') !!}", location: 'tc'
                    });
                });

            </script>
        @endif

        @if(\Illuminate\Support\Facades\Session::has('error'))
            <script type="text/javascript">
                $.growl.error({
                    message: "{!! \Illuminate\Support\Facades\Session::get('message') !!}", location: 'tc'
                });
            </script>
        @endif
    </header>
    @yield('content')
    @yield('script')
</div>
</body>
<script src="{{ asset('js/index.js') }}"></script>
<script src="{{asset('js/validate.js')}}"></script>
</html>