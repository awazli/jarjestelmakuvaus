<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'status'=>'Tila',
    'action'=>'Toiminnot',
    'pending'=>'Avoinna',
    'edit'=>'Muokkaa',
    'delete'=> 'Poista',
    'contact'=>'Yhteyshenkilö',
    'contact_no'=>'Puhelinnumero'
];
