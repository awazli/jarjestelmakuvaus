<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'en'=>'EN',
    'fr'=>'FR',
    'login' => 'Kirjaudu sisään',
    'email' => 'Sähköpostiosoite',
    'email_placeholder' => 'Kirjoita sähköpostiosoite',
    'failed' => 'Tarkista sähköpostiosoite.',
    'password' => 'Salasana',
    'password_placeholder' => 'Kirjoita salasana',
    'remember_me'=>'Muista minut ensikerralla',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'forgot_password' =>'Unohtuiko salasana?',
    'back' => 'Palaa takaisin',
    'save'=>'Tallenna',
    'go_back'=>'Palaa yleisnäkymään',
    'logout'=>'Kirjaudu ulos',
    'account_settings'=>'Asetukset'

];
