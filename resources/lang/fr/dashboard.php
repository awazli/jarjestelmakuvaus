<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'sales_terms'=>'Myyntiehdot -pohja',
    'terms'=>'Myyntiehdot',
    'deals'=>'Tarjoukset',
    'clients_management'=>'Asiakkaiden hallinta',
    'create_deal'=>'Luo tarjous',
    'all_deals'=>'Kaikki tarjoukset',
    'won_deals'=>'Voitetut tarjoukset',
    'lost_deals'=>'Hävityt tarjoukset',
    'pending_deals'=>'Avoinna tarjoukset',
    'project_title'=>'Otsikko',
    'client_name'=>'Asiakas',
    'close_date'=>'Deadline',
    'value'=>'Arvo',
    ''=>'',
];
