<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'add_client' => 'Luo uusi asiakas',
    'multiple_user'=>'Luo ensin uusi asiakas, jonka jälkeen voit luoda asiakkaan alle tarjouksia',
    'name' => 'Yhteyshenkilö',
    'company_name' => 'Yrityksen nimi',
    'vat_id'=>'Y-tunnus',
    'address'=>'Osoite',
    'billing_address'=>'Laskutusosoite',
    'create_client'=>'Luo asiakas',
    'clients'=>'Asiakas',
    'client'=>'Asiakas',
    'company'=>'Yritys',
    'edit_client'=>'Muokkaa asiakkaan tietoja',
    'update_client'=>'Tallenna muutokset'

];
