<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'status'=>'Status',
    'action'=>'Action',
    'pending'=>'Pending',
    'edit'=>'Edit',
    'delete'=>'Delete',
    'contact'=>'Contact',
    'contact_no'=>'Contact Number'
];
