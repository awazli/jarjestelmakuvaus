<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'add_client' => 'Add New Client',
    'multiple_user' => 'Multiple users? we offer custom tailored packages including premium support and detailed analytics.',
    'name' => 'Name',
    'company_name' => 'Company Name',
    'vat_id'=>'VAT ID',
    'address'=>'Address',
    'billing_address'=>'Billing Address',
    'create_client'=>'Create Client',
    'clients'=>'Clients',
    'client'=>'Client',
    'company'=>'Company',
    'edit_client'=>'Edit Client',
    'update_client'=>'Update Client'
];