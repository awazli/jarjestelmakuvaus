<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'sales_terms'=>'Sales Terms',
    'terms'=>'Terms',
    'deals'=>'Deals',
    'clients_management'=>'Clients Management',
    'create_deal'=>'Create Deal',
    'all_deals'=>'All Deals',
    'won_deals'=>'Won Deals',
    'lost_deals'=>'Lost Deals',
    'pending_deals'=>'Pending Deals',
    'project_title'=>'Project Title',
    'client_name'=>'Client name',
    'close_date'=>'Close date',
    'value'=>'Value',
];