<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'en'=>'EN',
    'fr'=>'FR',
    'login' => 'Log In',
    'email' =>'Email',
    'email_placeholder' => 'Please enter your email here.',
    'failed' => 'These credentials do not match our records.',
    'password'=>'Password',
    'password_placeholder' => 'XXXXXXX',
    'remember_me'=> 'Remember Me next time',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'forgot_password' =>'Forgot your password?',
    'back' => 'Back',
    'save'=>'Save',
    'go_back'=>'Go Back',
    'logout'=>'Logout',
    'account_settings'=>'Account Settings'

];
