<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/deal/list', 'HomeController@getDeals')->name('deal.list');

    Route::get('/settings', 'HomeController@settings')->name('settings.index');
    Route::post('/settings', 'HomeController@updateSettings')->name('settings.update');
    Route::get('/terms', 'HomeController@terms')->name('terms.index');
    Route::post('/terms', 'HomeController@updateTerms')->name('terms.update');

    Route::get('/clients', 'ClientController@index')->name('client.index');
    Route::get('/clients/list', 'ClientController@getClients')->name('client.list');
    Route::get('/client/create', 'ClientController@create')->name('client.create');
    Route::post('/client', 'ClientController@store')->name('client.store');
    Route::get('/client/{id}/edit', 'ClientController@edit')->name('client.edit');
    Route::put('/client/{id}', 'ClientController@update')->name('client.update');
    Route::get('/client/{id}', 'ClientController@destroy')->name('client.delete');

    Route::get('/deal/create', 'DealController@create')->name('deal.create');
    Route::post('/deal', 'DealController@store')->name('deal.store');
    Route::get('/deal/{id}/edit', 'DealController@edit')->name('deal.edit');
    Route::get('/deal/{id}/pdf', 'DealController@downloadPdf')->name('deal.pdf');
    Route::get('/deal/{id}/mail', 'DealController@sendEmail')->name('deal.email');
    Route::put('/deal/{id}', 'DealController@update')->name('deal.update');
    Route::get('/deal/{id}', 'DealController@show')->name('deal.show');
});